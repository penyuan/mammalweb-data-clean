# SPDX-License-Identifier: GPL-3.0+
# GNU GPLv3+ © 2019 Pen-Yuan Hsing

#
# Cleaning MammalWeb data
#

# BEFORE RUNNING, please specify the START_DATE and DUMP_DATE variables.
#
# This script takes raw MammalWeb data files exported from its MySQL database
# and produces cleaned up data frames for further analyses as CSV files. The
# data files needed are:
# `Animal.csv`
# `Options.csv`
# `Photo.csv`
# `Site.csv`
# `Upload.csv`
# This script needs, in the `data` folder (create it if it's not there), those
# CSV files. `Photo.csv` is the Photo table in the database with the "exif"
# column removed.
#
# Different major sections of this script are delineated by headers followed by
# a long string of "--------"s.
#
# The following files will be exported into the `output` directory upon a
# successful run of this script:
# (1) `final.classifications.csv` - sequence-level user-supplied classifications
# for each sequence with a corresponding gold standard including a column
# indicating whether a user classification was correct.
# (2) `final.classifications.noGS.csv` - same, but only user classifications
# *without* corresponding gold standards.
# 'final.gs.csv' - sequence-level gold standard classifications.
# (3) The cleaned Animal, Options, Photo, and Upload tables will also be
# exported as CSV files.

#
# Load required package(s) -----------------------------------------------------
#

# List package dependencies.
PACKAGES_LIST <- c("tidyverse", "lubridate")

# Set up Phil's function to install a list of packages.
checkInstalledPackages <- function(packages) {
    for (reqLib in packages) {
        isInstalled <- is.element(reqLib, installed.packages()[, 1])
        if (isInstalled == FALSE) {install.packages(reqLib)}
        require(reqLib, character.only = TRUE)
    }
}

# Install the list of required packages.
checkInstalledPackages(PACKAGES_LIST)

# Clean up.
rm(PACKAGES_LIST, checkInstalledPackages)

# this is a comment
# keep going!


#
# Set constants (start/end dates & GS users) -----------------------------------
#

# set start and data dump timestamps to bracket the time period to use for
# filtering data
START_DATE <- ymd("2015-03-27")
DUMP_DATE <- ymd_hms("2019-01-03 18:00:00") # timestamp corresponding to when the four data files below were dumped from MammalWeb

# Set list of gold standard users.
GS_USERS <- c(181, 182, 195, 397, 398, 428, 581)


#
# Read CSV data ----------------------------------------------------------------
#

# Check if data directory exists.
if (!dir.exists("data")) {
    cat("Data directory doesn't exist, please create and populate it first!\n")
}

# Read CSV data files, and show an error if at least one can't be read/found.
tryCatch({
    Animal <- read_csv(file = "data/Animal.csv", col_names = TRUE)
    Options <- read_csv(file = "data/Options.csv", col_names = TRUE)
    Photo <- read_csv(file = "data/Photo.csv", col_names = TRUE)
    Site <- read_csv(file = "data/Site.csv", col_names = TRUE)
    Upload <- read_csv(file = "data/Upload.csv", col_names = TRUE)
},
error = function(e) {
    cat("ERROR:", conditionMessage(e), "\n")
})


#
# Clean `Options` table --------------------------------------------------------
#

# Clean option names
Options$option_name <- as.character(Options$option_name)
Options$option_name[grep("Nothing", Options$option_name)] <- "Nothing"
Options$option_name[grep("Human", Options$option_name)] <- "Human"
Options$option_name[grep("Don't Know", Options$option_name)] <- "Don't Know"
Options$option_name[which(Options$option_id == 42)] <- "Small rodent"


#
# Clean `Site` table -----------------------------------------------------------
#

# Currently, this script only deals with the OS Grid References in the `Site`
# table. It imports a resolutions file for this purpose.

# Import resolutions CSV for `Site` table.
site_resolutions <- read_csv(file = "resolutions/site_resolutions.csv",
                             col_names = TRUE)
# In resolutions table, `site_id`s to remove from the main `Site` table are
# denoted with `-1` in the `person_id` column. So remove those from `Site`.
Site <- Site %>%
    filter(!site_id %in% filter(site_resolutions, person_id == -1)$site_id)
# Clean up.
rm(site_resolutions)


#
# Clean `Upload` table ---------------------------------------------------------
#

# process uploads table to remove invalid upload periods then apply this filter
# to photo/animal tables. See ?read_csv() for usage of "col_types" parameter.
# start by loading deployment resolutions table
upload_resolutions <- read_csv(file = "resolutions/deployment_resolutions.csv",
                               col_types = "iiiTTT")
# remove rows in `Upload` table that are in resolutions
Upload <- Upload %>%
    filter(!(upload_id %in% upload_resolutions$upload_id))
# remove from resolutions table rows where `person_id == -1` which is used to
# indicate invalid deployments we don't have enough information to adjudicate
upload_resolutions <- upload_resolutions %>%
    filter(person_id != -1)
# then apply resolutions table to `Upload`
Upload <- bind_rows(Upload, upload_resolutions) %>%
    arrange(upload_id, site_id, person_id, timestamp,
            deployment_date, collection_date)
# get deployment durations
Upload <- mutate(Upload, deploy_time = collection_date - deployment_date)
# are there durations shorter than 0?
upload.too.short <- filter(Upload, deploy_time < 0)
if (length(upload.too.short$upload_id > 0)) {
    # export list of too-short (negative length) deployments
    upload.too.short.properties <- upload.too.short %>%
        # get list of `photo_id`s for those deployments
        left_join(select(Photo, upload_id, photo_id, taken),
                  by = "upload_id") %>%
        arrange(upload_id, taken)
    # get intervals for those deployments and `photo_id` intervals within those
    # deployments
    upload.too.short.properties <- upload.too.short.properties %>%
        group_by(upload_id) %>%
        mutate(taken_begin = first(taken),
               taken_end = last(taken),
               n_photos = n_distinct(photo_id)) %>%
        ungroup() %>%
        mutate(taken_interval = interval(taken_begin, taken_end)) %>%
        mutate(deployment_interval = deployment_date %--% collection_date) %>%
        select(-photo_id, -taken) %>%
        distinct() %>%
        select(upload_id, site_id, person_id, timestamp, n_photos,
               deployment_date, collection_date, deployment_interval,
               taken_interval)
    # export list of these problematic deployments
    write_csv(x = upload.too.short.properties,
              path = "output/deployment_too_short.csv", col_names = TRUE)
    # throw error
    stop("New implausibly short deployment(s) (<0) detected. List exported to 'output'.")
} else {
    rm(upload.too.short)
}
# check for deployments longer than 6 months, which would be improbable
upload.too.long <- filter(Upload, deploy_time > 60 * 60 * 24 * 365 / 2)

# however, some deployments are really longer than 6 months after checking,
# which are kept in the resolutions table. So, remove from `upload.too.long`
# long deployments which are explicitly listed in the resolutions table.
upload.too.long <- upload.too.long %>%
    filter(!(upload_id %in% upload_resolutions$upload_id & person_id != -1))
# tabulate the remaining too-long deployments and throw an error
if (length(upload.too.long$upload_id > 0)) {
    # export list of too-long deployments
    upload.too.long.properties <- upload.too.long %>%
        # get list of `photo_id`s for those deployments
        left_join(select(Photo, upload_id, photo_id, taken),
                  by = "upload_id") %>%
        arrange(upload_id, taken)
    # get intervals for those deployments and `photo_id` intervals within those
    # deployments
    upload.too.long.properties <- upload.too.long.properties %>%
        group_by(upload_id) %>%
        mutate(taken_begin = first(taken),
               taken_end = last(taken),
               n_photos = n_distinct(photo_id)) %>%
        ungroup() %>%
        mutate(taken_interval = interval(taken_begin, taken_end)) %>%
        mutate(deployment_interval = deployment_date %--% collection_date) %>%
        select(-photo_id, -taken) %>%
        distinct() %>%
        select(upload_id, site_id, person_id, timestamp, n_photos,
               deployment_date, collection_date, deployment_interval,
               taken_interval)
    # export list of these problematic deployments
    write_csv(x = upload.too.long.properties,
              path = "output/deployment_too_long.csv", col_names = TRUE)
    # throw error
    stop("New improbably long deployment(s) (>6 months) detected. List exported to 'output'.")
} else {
    rm(upload.too.long)
}
# finally, identify any more problematic deployments that fall outside
# START_DATE and DUMP_DATE and remove them from `Upload`
Upload <- filter(Upload,
                 !(collection_date < START_DATE) & !(collection_date > DUMP_DATE))
# remove deployment resolutions table since it has been applied to `Upload`
rm(upload_resolutions)

#
# Clean `Photo` table ----------------------------------------------------------
#

# filter `photo` (and subsequently `animal`) to only keep photos from cleaned
# `upload` table
Photo <- filter(Photo, upload_id %in% Upload$upload_id)


#
# Clean `Animal` table ---------------------------------------------------------
#

# keep only `photo_id`s in `Animal` table that are in `Photo` table
Animal <- filter(Animal, photo_id %in% Photo$photo_id)
# update species column to be species names
Animal <- merge(Animal, Options[, c("option_id", "option_name")],
                by.x = 'species', by.y = 'option_id',
                all.x = T)
# remove "Like"s
Animal <- filter(Animal, option_name != "Like")
Animal <- arrange(Animal, photo_id, person_id, option_name)
# rename `option_name` to `Vote`, i.e. the species that a `person_id` voted for
# for each `photo_id`
Animal <- dplyr::rename(Animal, Vote = option_name)
# get `sequence_id`s from `Photo` into `Animal` table
Animal <- merge(Animal, Photo[, c("photo_id", "sequence_id")], all.x = T)
# there are a few extra "Vote"s for NA with corresponding real votes for an
# animal, so remove these extra NA votes
Animal <- filter(Animal, !(is.na(Vote)))
# sort columns
Animal <- arrange(Animal,
                  sequence_id,
                  photo_id,
                  person_id,
                  Vote)


#
# Create gold standard (GS) classifications table ------------------------------
#

# prepare gold standard classifications, which are classifications done by the
# following `person_id`s:
animal.gs <- subset(Animal, person_id %in% GS_USERS)
# import photo-level offline gold standard classifications CSVs
animal.gs.photo.offline <-
    read_csv("gs/gold_standard_offline_combined_2018-06.csv") %>%
    distinct()
## attach `sequence_id` to the imported offline gold standards
animal.gs.photo.offline <- animal.gs.photo.offline %>%
    left_join(select(Animal, photo_id, sequence_id), by = "photo_id") %>%
    distinct()
# arrange in ascending order
animal.gs.photo.offline <- arrange(animal.gs.photo.offline,
                                   sequence_id,
                                   photo_id,
                                   person_id)
# rename `species` column to `Vote` to match `animal.gs` and reorder columns to
# match it.
animal.gs.photo.offline <- animal.gs.photo.offline %>%
    dplyr::rename(Vote = species) %>%
    select(photo_id, person_id, Vote, sequence_id)
# note: some of the offline gold standard classifications are for sequences
# *not* in the cleaned `animal` table (probably because they fall outside valid
# deployment period, etc.), which shows up as `sequence_id == NA` in
# `animal.gs.photo.offline`, so remove those now:
animal.gs.photo.offline <- filter(animal.gs.photo.offline,
                                  !(is.na(sequence_id)))
# merge offline gold standard classifications into `animal.gs`
animal.gs <- bind_rows(animal.gs, animal.gs.photo.offline) %>%
    distinct()

# import and integrate photo-level conflict resolutions
animal.gs.photo.resolutions <-
    read_csv("resolutions/photo-level_resolutions.csv") %>%
    distinct()
# rename `species` column to `Vote` to match `animal.gs` and reorder columns to
# match it.
animal.gs.photo.resolutions <- select(animal.gs.photo.resolutions,
                                      photo_id,
                                      person_id,
                                      species,
                                      sequence_id) %>%
    distinct()
animal.gs.photo.resolutions <- dplyr::rename(animal.gs.photo.resolutions,
                                      Vote = species)
# remove and replace `photo_id`s in `animal.gs` with corresponding ones in
# resolutions table
animal.gs <-
    filter(animal.gs, !(photo_id %in% animal.gs.photo.resolutions$photo_id))
animal.gs <- bind_rows(animal.gs, animal.gs.photo.resolutions)
# clean up offline gold standard and photo-level conflict resolutions
rm(animal.gs.photo.offline, animal.gs.photo.resolutions)

# use above to make sequence-level gold standard classifications
animal.gs <- unique(animal.gs[, c("sequence_id", "person_id", "Vote")])

# import sequence-level gold standard conflict resolutions
animal.gs.seq.resolutions <-
    read_csv("resolutions/sequence-level_resolutions.csv") %>%
    distinct()
animal.gs.seq.resolutions <- dplyr::rename(animal.gs.seq.resolutions,
                                    Vote = species)
animal.gs.seq.resolutions <- select(animal.gs.seq.resolutions,
                                    sequence_id,
                                    person_id,
                                    Vote)
# remove and replace sequences in `animal.gs` with resolutions replacements
animal.gs <- filter(animal.gs,
                    !(sequence_id %in% animal.gs.seq.resolutions$sequence_id))
animal.gs <- bind_rows(animal.gs, animal.gs.seq.resolutions)
# clean up
rm(animal.gs.seq.resolutions)

# remove don't knows and others
animal.gs <- animal.gs %>%
    filter(!(Vote %in% c("Don't Know", "Other")))

# For each gold standard user, need to remove the "false nothings" - i.e. where
# there was nothing in one or more photos in a sequence, but also something in
# one or more
final.gs <- animal.gs %>%
    group_by(sequence_id, person_id) %>%
    mutate(nSppByUser = n_distinct(Vote)) %>%
    ungroup()
# temporary data frame for classifications for only one species within a
# sequence
final.gs.single.sp <- final.gs %>%
    filter(nSppByUser == 1)
# temporary data frame for classifications for multiple species within a
# sequence, and remove the Nothings
final.gs.multiple.spp <- final.gs %>%
    filter(nSppByUser > 1) %>%
    group_by(sequence_id, person_id) %>%
    # remove nothings from gold standard within each unique
    # `sequence_id`-`person_id` combination
    filter(Vote != "Nothing") %>%
    ungroup()

final.gs <- bind_rows(final.gs.multiple.spp, final.gs.single.sp)
final.gs <- select(final.gs,
                   -nSppByUser)

# clean up
rm(final.gs.multiple.spp, final.gs.single.sp)

# next, for each sequence (instead of for each gold standard user), also remove
# the vote(s) for nothing if there are other vote(s) for something. This means
# that instead of `group_by(sequence_id, person_id)`, now just
# `group_by(sequence_id)`
final.gs <- final.gs %>%
    group_by(sequence_id) %>%
    mutate(nSppInSeq = n_distinct(Vote)) %>%
    ungroup()
# temporary data frame for classifications for only one species within a
# sequence
final.gs.single.sp <- final.gs %>%
    filter(nSppInSeq == 1)
# temporary data frame for classifications for multiple species within a
# sequence, and remove the Nothings
final.gs.multiple.spp <- final.gs %>%
    filter(nSppInSeq > 1) %>%
    group_by(sequence_id) %>%
    # remove nothings from gold standard within each unique
    # `sequence_id`-`person_id` combination
    filter(Vote != "Nothing") %>%
    ungroup()

final.gs <- bind_rows(final.gs.multiple.spp, final.gs.single.sp)
final.gs <- select(final.gs,
                   -nSppInSeq) %>%
    arrange(sequence_id, person_id, Vote)
# clean up
rm(final.gs.multiple.spp, final.gs.single.sp)

# identify sequence-level gold standard conflicts
#
# start by getting list of sequences with more than one gold standard `person_id`
final.gs.multi.gs <- final.gs %>%
    group_by(sequence_id) %>%
    summarise(people = n_distinct(person_id)) %>%
    ungroup() %>%
    filter(people > 1)
final.gs.multi.gs <- final.gs %>%
    filter(sequence_id %in% final.gs.multi.gs$sequence_id)
final.gs.multi.gs <- arrange(final.gs.multi.gs,
                             sequence_id,
                             person_id,
                             Vote)
# create a column containing a list (in the form of a string) of species given
# each unique `sequence_id` and `person_id` combination
# what this means is that if the gold standard users fully agree on the species
# in a sequence, their respective lists of species should match exactly.
final.gs.multi.gs <- final.gs.multi.gs %>%
    group_by(sequence_id, person_id) %>%
    # spp will be the list of species a gold standard classifier gave a sequence
    mutate(spp = toString(Vote)) %>%
    ungroup()
# within each `sequence_id`, if each `person_id` agrees with each other, than
# there should only be one unique list of species for that sequence
final.gs.multi.gs <- final.gs.multi.gs %>%
    group_by(sequence_id) %>%
    # uniqueSppCombos should be exactly 1 if all users agree
    mutate(uniqueSppCombos = n_distinct(spp))
# filter for `uniqueSppCombos` > 1, if the result is not NULL, then there are
# still conflicts to resolve and throw and error with `stop()`
final.gs.multi.gs <- filter(final.gs.multi.gs,
                            uniqueSppCombos > 1)
if (dim(final.gs.multi.gs)[1] > 0) {
    stop("There are still sequence-level conflicts to resolve.")
} else {
    # if there are no conflicts, then remove `final.gs.multi`
    rm(final.gs.multi.gs)
}

# collapse `final.gs` to by keeping one set of classifications per sequence
# (instead of multiple sets from multiple gold standard users, which should be
# identical anyway)
final.gs <- select(final.gs,
                   sequence_id,
                   Vote) %>%
    distinct() %>%
    arrange(sequence_id, Vote)

# remove `animal.gs` since finalised sequence-level gold standard
# classifications are now in `final.gs`
rm(animal.gs)

# Further clean up `final.gs` by:
# (1) removing those from before START_DATE
final.gs <- left_join(final.gs,
                      select(Photo, sequence_id, photo_id, taken),
                      by = "sequence_id")
final.gs$taken <- ymd_hms(final.gs$taken) # parse timestamps for filtering by START_DATE
final.gs <- filter(final.gs, taken > START_DATE)
# (2) removing `sequence_id == -1`
final.gs <- filter(final.gs, sequence_id != -1)
# collapse back to sequence-level classifications
final.gs <- final.gs %>%
    select(sequence_id, Vote) %>%
    distinct()


#
# Create user-supplied classifications table -----------------------------------
#

# remove gold standard classifiers from `animal` so that it only contains
# classifications from users
animal.user <- subset(Animal, !person_id %in% GS_USERS)
# remove "Like"s, "Don't Know"s, and "Other"s
animal.user <- animal.user[animal.user$Vote != "Like",]
animal.user <- animal.user[animal.user$Vote != "Don't Know",]
animal.user <- animal.user[animal.user$Vote != "Other",]
# add columns to indicate if Spotter is an uploader of any sort and if they're
# the owner, get this information from `Photo` table where `person_id` means
# uploader
photo.uploaders <- Photo %>%
    select(photo_id, person_id) %>%
    dplyr::rename(Uploader = person_id)
animal.user <- left_join(animal.user, photo.uploaders, by = "photo_id")
animal.user <- dplyr::rename(animal.user, Spotter = person_id)
rm(photo.uploaders)

# sort/arrange columns in `animal.user` table
animal.user <- arrange(animal.user, sequence_id, photo_id, Spotter, Vote)
# collapse `animal.user` to sequence-level classifications
animal.user <- unique(animal.user[, c("Spotter", "Vote",
                                      "sequence_id", "Uploader")])

# For each user, need to remove the "false nothings" - i.e. where
# there was nothing in one or more photos in a sequence, but also something in
# one or more
animal.seq <- animal.user %>%
    group_by(sequence_id, Spotter) %>%
    mutate(nSppByUser = n_distinct(Vote)) %>%
    ungroup()
# temporary data frame for classifications for only one species within a
# sequence
animal.seq.single.sp <- animal.seq %>%
    filter(nSppByUser == 1)
# temporary data frame for classifications for multiple species within a
# sequence, and remove the Nothings
animal.seq.multiple.spp <- animal.seq %>%
    filter(nSppByUser > 1) %>%
    group_by(sequence_id, Spotter) %>%
    # remove nothings from gold standard within each unique
    # `sequence_id`-`person_id` combination
    filter(Vote != "Nothing") %>%
    ungroup()
animal.seq <- bind_rows(animal.seq.multiple.spp, animal.seq.single.sp)
animal.seq <- select(animal.seq,
                     -nSppByUser)
animal.seq <- arrange(animal.seq, sequence_id, Spotter, Vote)
# clean up
rm(animal.seq.multiple.spp, animal.seq.single.sp)

# split these sequence-level user classifications into one with a corresponding
# gold standard called `animal.seq`, and another without a gold standard called
# `final.classifications.noGS`
final.classifications.noGS <- subset(animal.seq,
                          !(sequence_id %in% final.gs$sequence_id)) %>%
    select(sequence_id, Uploader, Spotter, Vote)
animal.seq <- subset(animal.seq, sequence_id %in% final.gs$sequence_id)

# work out if the classifications are correct or not using gold standard
# data frame `final.gs`
# change column name in `final.gs` from `Vote` to `True.species`
final.gs <- dplyr::rename(final.gs, True.species = Vote)
# convert `final.gs` to base R data frame, which might affect running `merge()`
final.gs <- as.data.frame(final.gs)
# add a `Correct` column in preparation for merging with user classifications,
# and since `final.gs` is the gold standard, `Correct` will always be 1 here
final.gs$Correct <- 1
# with `aggregate()`, get the number of gold standard species in each sequence
nGS.per.seq <- aggregate(True.species ~ sequence_id, data = final.gs,
                         function(x) length(unique(x)))
# rename that columns from `True.species` to `nGSspp`
nGS.per.seq <- dplyr::rename(nGS.per.seq, nGSspp = True.species)
# merge `final.gs` into `animal.seq` so that we get a `Correct` column,
# note that this means all of the `Correct == 1`s in `animal.seq` are correct
# user classifications, and `Correct == NA` are incorrect
animal.seq <- merge(animal.seq, final.gs,
                    by.x = c("sequence_id", "Vote"),
                    by.y = c("sequence_id", "True.species"),
                    all.x = T)
# merge `nGS.per.seq` into `animal.seq` so that we get the `nGSspp` column which
# is the number of gold standard-identified species per sequence
animal.seq <- merge(animal.seq, nGS.per.seq, all.x = T)
# replace NAs with 0s
animal.seq <- replace_na(animal.seq, replace = list(Correct = 0))
# clean up
rm(nGS.per.seq)


#
# Process user classifications where GS species > 1 ----------------------------
#

# distinguish between cases where the number of GSspp was 1 and where the number
# of GSspp was > 1 and process them
# starting with processing cases where `nGSspp == 1`
animal.seq.singleGS <- subset(animal.seq, nGSspp == 1)
# `animal.seq.singleGS` is done needing no further analyses/action, so finalise
# it for use in the classifications table:
animal.seq.singleGS <- merge(select(animal.seq.singleGS, -nGSspp),
                             select(final.gs, -Correct),
                             all.x=T) # this just gets the needed columns
# put that info finalised classifications
final.classifications <- animal.seq.singleGS

# now deal with the thornier ones, i.e. sequences with >1 gold standard species
animal.seq.multipleGS <- subset(animal.seq, nGSspp > 1)
# get the count of user identified species per sequence and user
animal.seq.multipleGS.nUserspp <- aggregate(Vote ~ sequence_id + Spotter,
                                            data = animal.seq.multipleGS,
                                            function(x) length(unique(x)))
animal.seq.multipleGS.nUserspp <- dplyr::rename(animal.seq.multipleGS.nUserspp,
                                         nUserspp = Vote)
# put this information back into `animal.seq.multipleGS`
animal.seq.multipleGS <- merge(animal.seq.multipleGS,
                               animal.seq.multipleGS.nUserspp,
                               all.x=T)
rm(animal.seq.multipleGS.nUserspp)
# there are three cases for `animal.seq.multipleGS`:
# (i) "hard" cases where a user was incorrect for a given species they've
# identified AND there are multiple gold standard species. this means we will
# likely have to manually adjudicate these cases:
animal.seq.multipleGS.hard <- filter(animal.seq.multipleGS, Correct == 0)
# except one scenario, i.e. if the number of user classifications is 1
# (`nUserspp == 1`) and if it's "Nothing", then the user simply missed the gold
# standard species, so that can be algorithmically dealt with here:
animal.seq.multipleGS.hard.UsersppNothing <-
    filter(animal.seq.multipleGS.hard,
           nUserspp == 1 & Vote == "Nothing")
animal.seq.multipleGS.hard.UsersppNothing <-
    animal.seq.multipleGS.hard.UsersppNothing %>%
    left_join(select(final.gs, -Correct), by = "sequence_id")
animal.seq.multipleGS.hard.UsersppNothing <-
    animal.seq.multipleGS.hard.UsersppNothing %>%
    select(-nUserspp, -nGSspp) %>%
    select(sequence_id, Vote, Spotter, Uploader, Correct, True.species)
final.classifications <- bind_rows(final.classifications,
                                   animal.seq.multipleGS.hard.UsersppNothing)
# apply resolutions table where some cases have been manually adjudicated
animal.seq.multipleGS.hard.resolutions <-
    read_csv("resolutions/animal.seq.multipleGS.hard_resolutions.csv",
             col_types = "iciiic")
final.classifications <- final.classifications %>%
    bind_rows(animal.seq.multipleGS.hard.resolutions)
animal.seq.multipleGS.hard <- animal.seq.multipleGS.hard %>%
    filter(!(sequence_id %in% animal.seq.multipleGS.hard.resolutions$sequence_id))
rm(animal.seq.multipleGS.hard.resolutions)
# if there are still any "hard" ones left, throw error for manual processing
animal.seq.multipleGS.hard <- filter(animal.seq.multipleGS.hard,
                                     !(sequence_id %in% animal.seq.multipleGS.hard.UsersppNothing$sequence_id))
if (dim(animal.seq.multipleGS.hard)[1] > 0) {
    stop("Need to check `animal.seq.multipleGS.hard` where user was incorrect with nGSspp > 1.")
} else {
    rm(animal.seq.multipleGS.hard, animal.seq.multipleGS.hard.UsersppNothing)
}
# (ii) if nGSspp == nUserspp and Correct == 1, then it's done, so those
# can be joined with animal.seq.singleGS to make final.classifications
animal.seq.multipleGS.equal <- subset(animal.seq.multipleGS,
                                      nGSspp == nUserspp & Correct == 1)
animal.seq.multipleGS.equal <- animal.seq.multipleGS.equal %>%
    select(-nGSspp, -nUserspp)
animal.seq.multipleGS.equal <- animal.seq.multipleGS.equal %>%
    select(sequence_id, Vote, Spotter, Uploader, Correct)
# since all rows are `Correct == 1`, just copy `Vote` into `True.species`
animal.seq.multipleGS.equal <- animal.seq.multipleGS.equal %>%
    mutate(True.species = Vote)
# re-order columns to match `final.classifications` then merge into it
animal.seq.multipleGS.equal <- animal.seq.multipleGS.equal %>%
    select(sequence_id, Vote, Spotter, Uploader, Correct, True.species)
final.classifications <- bind_rows(final.classifications,
                                   animal.seq.multipleGS.equal)
rm(animal.seq.multipleGS.equal)

# then, we need to deal with user classifications where the count of
# user-identified species are not equal to the count of gold-standard-identified
# species:
# (iii) i.e. where a user correctly identified a species but missed others
# that the gold standard identified. In other words, I need to add to
# `animal.seq.multipleGS` rows where `Correct == 0`, `Vote == "Nothing"`, and
# `True.species` is the gold standard species that's missed by the user. add a
# `True.species` column
animal.seq.multipleGS.unequal <- filter(animal.seq.multipleGS,
                                        nGSspp != nUserspp & Correct == 1)
# remove extra columns
animal.seq.multipleGS.unequal <- select(animal.seq.multipleGS.unequal,
                                        -Correct, -nGSspp, -nUserspp)
# merge in `True.species`
animal.seq.multipleGS.unequal <- left_join(animal.seq.multipleGS.unequal,
                                           select(final.gs, -Correct),
                                           by = "sequence_id")
# reconstruct `Correct` by comparing with `True.species`
animal.seq.multipleGS.unequal <- mutate(animal.seq.multipleGS.unequal,
                                        Correct = if_else(Vote == True.species,
                                                          1, 0))
# the `Correct == 1`s are fine, but need to change the `Vote`s for when `Correct
# == 0` to "Nothing", do this by first removing the `Vote` column, and
# reconstructing it from `True.species` and `Correct`
animal.seq.multipleGS.unequal <- animal.seq.multipleGS.unequal %>%
    select(-Vote)
animal.seq.multipleGS.unequal <- animal.seq.multipleGS.unequal %>%
    mutate(Vote = if_else(Correct == 1, True.species, "Nothing"))
# re-order columns
animal.seq.multipleGS.unequal <- animal.seq.multipleGS.unequal %>%
    select(sequence_id, Vote, Spotter, Uploader, Correct, True.species)
# bind it into `final.classifications`
final.classifications <- bind_rows(final.classifications,
                                   animal.seq.multipleGS.unequal)
# clean up
rm(animal.seq.multipleGS.unequal)
rm(animal.seq.singleGS, animal.seq.multipleGS)
rm(animal.seq, animal.user)

final.gs <- final.gs %>%
    select(-Correct)


#
# Anonymise `person_id`s -------------------------------------------------------
#

# anonymise the person IDs, as follows:
person_ids <- unique(c(final.classifications$Spotter, final.classifications$Uploader))
person_ids <- as.data.frame(person_ids)
person_ids$new_id <- 1000:(999+dim(person_ids)[1])
final.classifications$Spotter <- person_ids$new_id[match(final.classifications$Spotter,
                                                         person_ids$person_ids)]
final.classifications$Uploader <- person_ids$new_id[match(final.classifications$Uploader,
                                                          person_ids$person_ids)]
rm(person_ids)

# now, `final.classifications` should have everything, just sort the columns
final.classifications <- final.classifications %>%
    arrange(sequence_id, Spotter, Vote, Correct, True.species, Uploader) %>%
    select(sequence_id, Uploader, Spotter, Vote, Correct, True.species)


#
# Export data ------------------------------------------------------------------
#

# Create an output folder if it doesn't already exist.
dir.create(path = "output", showWarnings = FALSE)
# Export cleaned up data frames.
write_csv(x = Animal, file = "output/Animal.csv", col_names = TRUE)
write_csv(x = Photo, file = "output/Photo.csv", col_names = TRUE)
write_csv(x = Options, file = "output/Options.csv", col_names = TRUE)
write_csv(x = Site, file = "output/Site.csv", col_names = TRUE)
write_csv(x = Upload, file = "output/Upload.csv", col_names = TRUE)
# Export cleaned up Spotter and gold standard classifications.
write_csv(x = final.classifications, # classifications w/ corresponding GS
          file = "output/final.classifications.csv", col_names = TRUE)
write_csv(x = final.classifications.noGS, # classifications w/ no corresponding GS
          file = "output/final.classifications.noGS.csv", col_names = TRUE)
write_csv(x = final.gs, # GS classifications
          file = "output/final.gs.csv", col_names = TRUE)
