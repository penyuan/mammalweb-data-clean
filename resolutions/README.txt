The CSV files in this folder are adjudications for cleaning raw MammalWeb data.

As of 2019-09-16, the following files are up to date:

deployment_resolutions.csv (for cleaning Upload table)
photo_level_resolutions.csv (for cleaning photo-level gold standard classifications)
sequence_level_resolutions.csv (for cleaning sequence-level gold standard classifications)
site_resolutions.csv (for cleaning the Upload table)

In these files, when `person_id == -1`, that means the corresponding records will be removed from their respective tables.

There is also the file `animal.seq.multipleGS.hard_resolutions.csv`, which is to adjudicate cases where a user was incorrect for a given species they've identified AND there are multiple gold standard species for that sequence. This is dealt with in the `animal.seq.multipleGS.hard` part of the cleaning code. Note that as of 2019-01-07, after applying this table, the data frame of user classifications `final.classifications` will have cases where e.g., the user Spotted one species incorrectly when there are two gold standard species in the sequence, which leads to two rows where Correct == 0, each corresponding to one of the gold standard species.
