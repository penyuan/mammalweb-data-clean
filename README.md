# MammalWeb data cleaning script

> [R](https://en.wikipedia.org/wiki/R_(programming_language)) script to clean up raw data from [MammalWeb](https://www.MammalWeb.org/)'s [MySQL](https://en.wikipedia.org/wiki/MySQL) [database](https://en.wikipedia.org/wiki/Relational_database_management_system).

This [git](https://en.wikipedia.org/wiki/Git) repository contains an [R](https://en.wikipedia.org/wiki/R_(programming_language)) script and associated [RStudio](https://en.wikipedia.org/wiki/RStudio) project file to clean up raw data from [MammalWeb](https://www.MammalWeb.org/)'s [MySQL](https://en.wikipedia.org/wiki/MySQL) [database](https://en.wikipedia.org/wiki/Relational_database_management_system).

This raw data is exported from tables in the database into [CSV](https://en.wikipedia.org/wiki/CSV) files, which the R script in this repository reads. The script attempts to correct absurd entries in the data, applies a manual set of resolutions (to entries that can't be algorithmically adjudicated), and exports the cleaned data into CSV files for downstream analyses. User IDs (`person_id`s) of non-gold-standard users will be anonymised, too.

In addition, this script tabulates the classifications, or "votes", cast for each photo sequence by MammalWeb users. When a corresponding gold standard classification (collated from online and offline sources) is available for a sequence, it is listed next to the user classifications.

Importantly, this repository also contains the latest canonical set of (1) offline gold standard classifications, and (2) manual "resolutions" tables that adjudicate/clean data that the script cannot algorithmically handle. If (1) and/or (2) are updated, please upload them to this repository.

## Install

1. Install the latest version of [R](https://en.wikipedia.org/wiki/R_(programming_language)). Version 3.5.2 has been tested to work.
2. Install the latest version of [RStudio](https://en.wikipedia.org/wiki/RStudio). At time of writing, an installer can be downloaded from [here](https://www.rstudio.com/products/rstudio/download/#download). Version 1.1.463 has been tested to work.
3. Download (or in git-speak, *[clone](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)*) this repository onto your computer. This step can also be done within RStudio (I think that is the best way), in which case please use the name of this repository (`mammalweb-data-clean`) as the RStudio project name.
4. (optional) Within RStudio, install the `tidyverse` and `lubridate` packages, though the script will try to install them if you haven't. At time of writing, `tidyverse` 1.2.1 and `lubridate` 1.7.4 are known to work.
5. Create a new, empty directory named `data` on the same level as the main script `mammalweb-data-clean.R`. This is where the raw CSVs from the MammalWeb database should be placed (they are not included in this repository).

## Usage

1. If it has not already been done, please export the following data tables from the MammalWeb MySQL database: `Animal`, `Options`, `Photo`, `Site`, and `Upload`. They should be exported as CSV files with headers included (i.e., first row in the CSVs should be headers/names of columns) into `Animal.csv`, `Options.csv`, `Photo.csv`, `Site.csv`, and `Upload.csv`. **IMPORTANT**: When exporting the `Photo` table, please *exclude* the column named `exif` otherwise the exported file will be very large and unwieldy!
2. Place those CSV files into the `data` directory.
3. Once done, the file and directory structure should look like this:

```
mammalweb-data-clean
├── data
│   ├── Animal.csv
│   ├── Options.csv
│   ├── Photo.csv
│   ├── Site.csv
│   └── Upload.csv
├── gs
│   └── gold_standard_offline_combined_2018-06.csv
├── LICENSE
├── MammalWeb-data-clean.R
├── mammalweb-data-clean.Rproj
├── README.md
└── resolutions
    ├── animal.seq.multipleGS.hard_resolutions.csv
    ├── deployment_resolutions.csv
    ├── photo-level_resolutions.csv
    ├── README.txt
    ├── sequence-level_resolutions.csv
    └── site_resolutions.csv
```

4. At this point, the easiest way to run this script is to open the corresponding project file - `mammalweb-data-clean.Rproj` - in RStudio, which would set the [working directory](https://support.rstudio.com/hc/en-us/articles/200711843-Working-Directories-and-Workspaces) appropriately. The cleaning script (`mammalweb-data-clean.R`) is now almost ready to be run.
5. **IMPORTANT**: Before running, please make sure that the start and end timestamps are set correctly. This is done by setting the `START_DATE` and `DUMP_DATE` variables near the beginning of the script. Usually, you won't need to change `START_DATE` since it corresponds to the beginning of the MammalWeb project, and `DUMP_DATE` should be set to the timestamp of when you exported the data from the database. Note that the cleaning script will *only* keep data that falls between `START_DATE` and `DUMP_DATE`, which will affect *all* downstream analyses. So think carefully.
6. Run the script in its entirety.

**NOTES**:

* One of the most common sources of errors is that, as more data (photos or classifications) are added to MammalWeb, there would be new erroneous entries in the data that the script cannot clean up on its own. Typically the script would print errors along the lines of (but not limited to) "New implausibly short deployment (<0) detected" or "There are still sequence-level conflicts to resolve." If so, the resolutions tables in the `resolutions` directory might need to be updated to correct those entries.
* The `gs` directory contains the file `gold_standard_offline_combined_2018-06.csv`. This contains sequence-level gold standard classifications done by Derek, Pen, etc. If more are done in the future, please append them to this file.
* There are several files in the `resolutions` directory:
    * `animal.seq.multipleGS.hard_resolutions.csv` are gold standard classifications for difficult sequences where multiple species were identified in the same sequences by both regular and expert (gold standard) classifiers.
    * `deployment_resolutions.csv` contains replacements for incorrect entries in the `Upload` table, matched by `upload_id`.
    * `photo-level_resolutions.csv` are adjudications for when gold standard classifiers disagree on what's in a photo.
    * `sequence-level_resolutions.csv` are the same but for sequences.
    * `site_resolutions.csv` is for cleaning up the `Site` table, e.g., when the coordinates have been entered incorrectly.
    * `README.txt` contains a few more details.
* The script is split into several sections. The beginning of each section is delineated by a header followed by a series of `---`s, such as:
```
#
# Load required package(s) -----------------------------------------------------
#
```
Within RStudio, these sections can be folded which might aid reading and navigation (there are some "sub" headers without the `---`s, which do not count as section deliminators). RStudio also provides a table-of-contents-like view of the script based on these sections.


## Expected output

If the script ran without errors, then it will create a directory called `output` and place the following files in it:

```
├── output
│   ├── Animal.csv
│   ├── final.classifications.csv
│   ├── final.classifications.noGS.csv
│   ├── final.gs.csv
│   ├── Options.csv
│   ├── Photo.csv
│   ├── Site.csv
│   └── Upload.csv
```

**What are these files?**

* `Animal.csv`, `Options.csv`, `Photo.csv`, `Site.csv`, and `Upload.csv` are cleaned versions of the data, and can be used for further analyses.
* `final.gs.csv` contains sequence-level gold standard classifications.
* `final.classifications.csv` and `final.classifications.noGS.csv` contain sequence-level classifications provided by non-gold-standard users (i.e., Spotters) with and without corresponding gold standards, respectively. In the latter case, it is simply because we don't have gold standard classifications for those sequences.

This output can also be used to produce the dataset which the consensus classification code published in [Hsing (2018)](https://gitlab.com/penyuan/consensus_classifications_MammalWeb) requires, in which case just rename `final.classifications.csv` produced by this script into `classifications.csv` and place it into the `data` folder for the consensus classification code.

At the end, the file and directory structure should look like this:

```
mammalweb-data-clean
├── data
│   ├── Animal.csv
│   ├── Options.csv
│   ├── Photo.csv
│   ├── Site.csv
│   └── Upload.csv
├── gs
│   └── gold_standard_offline_combined_2018-06.csv
├── LICENSE
├── MammalWeb-data-clean.R
├── mammalweb-data-clean.Rproj
├── output
│   ├── Animal.csv
│   ├── final.classifications.csv
│   ├── final.classifications.noGS.csv
│   ├── final.gs.csv
│   ├── Options.csv
│   ├── Photo.csv
│   ├── Site.csv
│   └── Upload.csv
├── README.md
└── resolutions
    ├── animal.seq.multipleGS.hard_resolutions.csv
    ├── deployment_resolutions.csv
    ├── photo-level_resolutions.csv
    ├── README.txt
    ├── sequence-level_resolutions.csv
    └── site_resolutions.csv
```

## Contributing

Please send pull requests updating the script itself or the gold standard and resolution CSVs.

Small note: If editing this README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

[GNU GPLv3 or later © 2019 Pen-Yuan Hsing](../LICENSE)